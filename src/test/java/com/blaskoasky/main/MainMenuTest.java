package com.blaskoasky.main;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.blaskoasky.Constant.*;

class MainMenuTest {

    MainMenu menu;

    @BeforeEach
    void setup() {
        menu = new MainMenu();
    }

    @Test
    void tryWriteMMM() {
        Assertions.assertDoesNotThrow(() -> menu.tryWriteMMM(SAVE_MMM_PATH));
    }

    @Test
    void tryWriteModus() {
        Assertions.assertDoesNotThrow(() -> menu.tryWriteModus(SAVE_MODUS_PATH));
    }
}