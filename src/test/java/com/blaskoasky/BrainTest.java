package com.blaskoasky;

import org.junit.jupiter.api.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class BrainTest {

    Brain brain;
    ArrayList listArr1, listArr2, listNullArr = null;
    Map<Integer, Integer> mMap1, mMap2;
    String rightReadPath = "src/main/resources/data_sekolah.csv";
    String wrongReadPath = "ini/wrong/path/yak";
    String saveMMMPath = "src/main/resources/MeanMedianModus.txt";
    String saveModusSekolahPath = "src/main/resources/ModusSekolah.txt";

    @BeforeEach
    void setup() {
        brain = new Brain();
        declareListAngka();
    }

    void declareListAngka() {

        // Mean    : 16.75
        // Median  : 15.5
        // Modus   : 13
        Integer[] arrList1 = {9, 10, 12, 13, 13, 13, 15, 15, 16, 16, 18, 22, 23, 24, 24, 25};
        listArr1 = new ArrayList<>(Arrays.asList(arrList1));

        // Mean    : 8.32 (rounded up)
        // Median  : 8.0
        // Modus   : 7
        Integer[] arrList2 = {
                5, // @1
                6, 6, // @2
                7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
                7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, // @62
                8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
                8, 8, 8, 8, 8, 8, 8, 8, // @42
                9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
                9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, // @50
                10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
                10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 // @41
        };
        listArr2 = new ArrayList<>(Arrays.asList(arrList2));
    }

    @Test
    @DisplayName("Positive TestCase - mean() - Check if Mean formula is right.")
    void positiveMean() {
        var expectedMean1 = 16.75d;
        var expectedMean2 = 8.32d;
        Assertions.assertEquals(expectedMean1, brain.mean(listArr1));
        Assertions.assertEquals(expectedMean2, brain.mean(listArr2));
    }

    @Test
    @DisplayName("Negative TestCase - mean() - Check if given ArrayList is Null.")
    void negativeMean() {
        Assertions.assertThrows(NullPointerException.class, () -> brain.mean(listNullArr));
    }

    @Test
    @DisplayName("Positive TestCase - median() - Check if Median formula is right.")
    void positiveMedian() {
        var expectedMedian1 = 15.5d;
        var expectedMedian2 = 8.0d;
        Assertions.assertEquals(expectedMedian1, brain.median(listArr1));
        Assertions.assertEquals(expectedMedian2, brain.median(listArr2));
    }

    @Test
    @DisplayName("Negative TestCase - median() - Check if given ArrayList is Null.")
    void negativeMedian() {
        Assertions.assertThrows(NullPointerException.class, () -> brain.median(listNullArr));
    }

    @Test
    @DisplayName("Positive TestCase - modus() - Check if Modus formula is right.")
    void positiveModus() {
        var expectedModus1 = 13;
        var expectedModus2 = 7;
        Assertions.assertEquals(expectedModus1, brain.modus(listArr1));
        Assertions.assertEquals(expectedModus2, brain.modus(listArr2));
    }

    @Test
    @DisplayName("Negative TestCase - modus() - Check if given ArrayList is Null.")
    void negativeModus() {
        Assertions.assertThrows(NullPointerException.class, () -> brain.modus(listNullArr));
    }

    @Test
    @DisplayName("Positive TestCase - modusSekolah() - Check if Modus Sekolah formula is right.")
    void positiveModusSekolah() {
        declareMap();
        Assertions.assertEquals(mMap1, brain.modusSekolah(listArr1));
        Assertions.assertEquals(mMap2, brain.modusSekolah(listArr2));
    }

    @Test
    @DisplayName("Negative TestCase - modusSekolah() - Check if given ArrayList is Null.")
    void negativeModusSekolah() {
        Assertions.assertThrows(NullPointerException.class, () -> brain.modusSekolah(listNullArr));
    }

    void declareMap() {
        // 16=2, 18=1, 22=1, 23=1, 24=2, 9=1, 25=1, 10=1, 12=1, 13=3, 15=2}
        Integer[] keys1 = {9, 10, 12, 13, 15, 16, 18, 22, 23, 24, 25};
        Integer[] values1 = {1, 1, 1, 3, 2, 2, 1, 1, 1, 2, 1};
        mMap1 = IntStream.range(0, keys1.length).boxed()
                .collect(Collectors.toMap(i -> keys1[i], i -> values1[i]));

        // {5=1, 6=2, 7=62, 8=42, 9=50, 10=41}
        Integer[] keys2 = {5, 6, 7, 8, 9, 10};
        Integer[] values2 = {1, 2, 62, 42, 50, 41};
        mMap2 = IntStream.range(0, keys2.length).boxed()
                .collect(Collectors.toMap(i -> keys2[i], i -> values2[i]));
    }

    @Test
    @DisplayName("Positive TestCase - readFile() - Check if not throw and returned a list")
    void positiveReadFile() {
        checkIfReadFileNotThrow();
        checkIfReadFileWorks_ReturnArrayList();
    }

    void checkIfReadFileNotThrow() {
        Assertions.assertDoesNotThrow(() -> brain.readFile(rightReadPath));
    }

    void checkIfReadFileWorks_ReturnArrayList() {
        try {
            Assertions.assertEquals(listArr2, brain.readFile(rightReadPath));
        } catch (IOException e) {

        }
    }

    @Test
    @DisplayName("Negative TestCase - readFile() - Check if given PATH is wrong")
    void negativeReadFile() {
        Assertions.assertThrows(IOException.class, () -> brain.readFile(wrongReadPath));
    }

    @Test
    @DisplayName("Positive TestCase - writeMMMFile() - Check if not throw and file is created")
    void positiveWriteMMMFile() {
        checkIfWriteMMMFileNotThrow();
        runWriteMMMFile();
        checkIfMMMFileIsCreated();
    }

    void checkIfMMMFileIsCreated() {
        File file = new File(saveMMMPath);
        Assertions.assertTrue(file.exists());
    }

    void runWriteMMMFile() {
        try {
            brain.writeMMMFile(saveMMMPath, brain.readFile(rightReadPath));
        } catch (IOException e) {

        }
    }

    void checkIfWriteMMMFileNotThrow() {
        Assertions.assertDoesNotThrow(() -> brain.writeMMMFile(saveMMMPath, brain.readFile(rightReadPath)));
    }

    @Test
    @DisplayName("Negative TestCase - writeMMMFile() - check if given ArrayList is null")
    void negativeWriteMMMFile() {
        Assertions.assertThrows(Exception.class, () -> brain.writeMMMFile(saveMMMPath, listNullArr));
    }

    @Test
    @DisplayName("Positive TestCase - writeModusFile() - Check if not throw and file is created")
    void positiveWriteModusSekolahFile() {
        checkIfWriteModusFileNotThrow();
        runWriteModusFile();
        checkIfWriteModusileIsCreated();
    }

    void checkIfWriteModusileIsCreated() {
        File file = new File(saveModusSekolahPath);
        Assertions.assertTrue(file.exists());
    }

    void runWriteModusFile() {
        try {
            brain.writeModusFile(saveModusSekolahPath, brain.readFile(rightReadPath));
        } catch (IOException e) {

        }
    }

    void checkIfWriteModusFileNotThrow() {
        Assertions.assertDoesNotThrow(() -> brain.writeModusFile(saveMMMPath, brain.readFile(rightReadPath)));
    }

    @Test
    @DisplayName("Negative TestCase - writeModusFile() - check if given ArrayList is null")
    void negativeWriteModusFile() {
        Assertions.assertThrows(Exception.class, () -> brain.writeModusFile(saveMMMPath, listNullArr));
    }

}