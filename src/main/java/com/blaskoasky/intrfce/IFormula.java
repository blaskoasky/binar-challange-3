package com.blaskoasky.intrfce;

import java.util.List;
import java.util.Map;

public interface IFormula {

    double mean(List<Integer> list);

    double median(List<Integer> list);

    int modus(List<Integer> list);

    Map<Integer, Integer> modusSekolah(List<Integer> list);
}
