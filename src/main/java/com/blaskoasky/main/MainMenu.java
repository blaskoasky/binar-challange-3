package com.blaskoasky.main;

import com.blaskoasky.Brain;
import com.blaskoasky.intrfce.IMenuFuctional;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

import static com.blaskoasky.Constant.*;


public class MainMenu {

    Brain brain = new Brain();
    Scanner scan = new Scanner(System.in);

    public void switchMenu(int pilih) {

        // menuFuctional.showMenu();
        // var pilih = mintaInput();

        switch (pilih) {
            case 1:
                tryWriteMMM(SAVE_MMM_PATH);
                // menuBack()
                break;
            case 2:
                tryWriteModus(SAVE_MODUS_PATH);
                // menuBack()
                break;
            case 3:
                tryWriteMMM(SAVE_MMM_PATH);
                tryWriteModus(SAVE_MODUS_PATH);
                // menuBack()
                break;
            case 0:
                System.out.println("\nKeluar Console....");
                System.exit(0);
                break;
            default:
                System.out.println("\n\nPILIH ANGKA YANG BENAR!!!\n\n");
                switchMenu(menuFuctional.showMenu());
                // menuBack(pilih);
                break;
        }
    }

    IMenuFuctional menuFuctional = () -> {
        System.out.println("\n*************************************************");
        System.out.println("=================================================");
        System.out.println("Aplikasi Mengolah Data Nilai Siswa");
        System.out.println("-------------------------------------------------");
        System.out.println("1. Menghitung Mean-Median-Modus");
        System.out.println("2. Menghitung Modus Sekolah");
        System.out.println("3. Jalankan dua program diatas");
        System.out.println("0. Exit");
        System.out.println("=================================================");
        System.out.print("Masukkan Pilihan: ");
        return mintaInput();
    };

    public int mintaInput() {
        int pilih = 0;
        try {
            pilih = scan.nextInt();
            /*if (pilih >= 4) {
                pilih = 0;
            }*/
        } catch (InputMismatchException e) {
            System.out.println("Masukkan ANGKA INTEGER!");
        }
        return pilih;
    }

    public void tryWriteMMM(String save) {
        // brain.writeMMMFile(SAVE_MMM_PATH, brain.readFile(READ_FILE));
        try {
            brain.writeMMMFile(save, brain.readFile(READ_FILE));
        } catch (IOException e) {
            System.out.println("\n\n*** File tidak ditemukan ***\n\n");
            // menuBack();
        }

    }

    public void tryWriteModus(String save) {
        // brain.writeModusFile(SAVE_MODUS_PATH, brain.readFile(READ_FILE));
        try {
            brain.writeModusFile(save, brain.readFile(READ_FILE));
        } catch (IOException e) {
            System.out.println("\n\n*** File tidak ditemukan ***\n\n");
            // menuBack();
        }
    }

    /*public void menuBack(int pilih) {
        System.out.println("\n\nKembali ....");
        System.out.println("=================================================");
        System.out.println("0. Exit");
        System.out.println("1. Kembali ke menu utama");
        System.out.print("Masukkan Pilihan: ");

        switch (pilih) {
            case 0:
                System.out.println("\nKeluar Console....");
                System.exit(0);
                break;
            case 1:
                menuFuctional.showMenu();
                break;
            default:
                System.out.println("\n\nPILIH ANGKA YANG BENAR!!!\n\n");
                menuBack(pilih);
                break;
        }
    }*/
}
