package com.blaskoasky;

public class Constant {
    public static final String READ_FILE = "src/main/resources/data_sekolah.csv";
    public static final String SAVE_MMM_PATH = "src/main/resources/MeanMedianModus.txt";
    public static final String SAVE_MODUS_PATH = "src/main/resources/ModusSekolah.txt";
}
